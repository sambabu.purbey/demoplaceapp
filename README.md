# DemoTest
A sample application in MVVM and SWIFT 5 demonstrating how to get started unit test cases.

This demo test from place listing with Json.

Some things in Demo have simple city listing 
Model integrate from place json 
TDD integration 


##Goal

The goal of this demo app is to practice a style of experimental model. Look for as many different ways of handling the issues as possible. Consider the various tradeoffs of each. What techniques are best for exploring these models.

