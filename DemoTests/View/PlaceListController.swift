
//
//  PlaceListController.swift
//  DemoTests
//
//  Created by Laksh Purbey on 10/28/20.
//  Copyright © 2020 Laksh Purbey. All rights reserved.
//


import UIKit

class PlaceListController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = PlaceListViewModel(dataFetcher: PlaceDataFetcher())

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        headerLabel.text = viewModel.title
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PlaceTableCell
        let cellDataModel = viewModel.tableCellDataModelForIndexPath(indexPath)
        cell.configureCell(model: cellDataModel)
        return cell
    }
    
}

